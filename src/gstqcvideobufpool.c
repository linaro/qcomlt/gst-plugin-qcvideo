/*
 * GStreamer
 * Copyright (C) 2005 Thomas Vander Stichele <thomas@apestaart.org>
 * Copyright (C) 2005 Ronald S. Bultje <rbultje@ronald.bitfreak.net>
 * Copyright (C) 2014 c_kamuju <<user@hostname.org>>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:element-qcvideobufpool
 *
 * FIXME:Describe qcvideobufpool here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! qcvideobufpool ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gst/gst.h>

#include "gstqcvideobufpool.h"

#define LUMA_PLANE 0
#define CHROMA_PLANE 1

#define gst_qcvideobufpool_parent_class parent_class
G_DEFINE_TYPE (Gstqcvideobufpool, gst_qcvideobufpool, GST_TYPE_BUFFER_POOL);

/* GObject vmethod implementations */

static void
qc_video_buffer_pool_free (GstBufferPool * bpool, GstBuffer * buffer)
{
  gst_buffer_unref (buffer);
}

static void
qc_video_buffer_reset_buffer (GstBufferPool * bpool, GstBuffer * buffer)
{
  gst_buffer_remove_memory (buffer, -1);
  GST_BUFFER_FLAG_UNSET (buffer, GST_BUFFER_FLAG_TAG_MEMORY);
}

GstFlowReturn
qc_video_buffer_pool_alloc_buffer (GstBufferPool * bpool,
    GstBuffer ** buffer, GstBufferPoolAcquireParams * params)
{
  GstBuffer *newbuf = NULL;
  gint chroma_offset = 0;
  gint chroma_alignment = 0;
  Gstqcvideobufpool *pool = GST_QCVIDEOBUFPOOL (bpool);

  chroma_alignment =
      GST_ROUND_UP_128 (pool->info.width) * GST_ROUND_UP_32 (pool->info.height);
  chroma_offset = GST_ROUND_UP_N (chroma_alignment, 8192);
  pool->info.offset[CHROMA_PLANE] = chroma_offset;
  newbuf = gst_buffer_new ();
  gst_buffer_add_video_meta_full (newbuf, GST_VIDEO_BUFFER_FLAG_INTERLACED,
      GST_VIDEO_INFO_FORMAT (&pool->info),
      GST_VIDEO_INFO_WIDTH (&pool->info),
      GST_VIDEO_INFO_HEIGHT (&pool->info),
      GST_VIDEO_INFO_N_PLANES (&pool->info), pool->info.offset,
      pool->info.stride);
  *buffer = newbuf;
  GST_DEBUG ("\n  pool_alloc_buffer : %p\n", newbuf);
  return GST_FLOW_OK;
}

gboolean
qc_video_buffer_pool_set_config (GstBufferPool * bpool, GstStructure * config)
{
  GstAllocator *allocator = NULL;

  Gstqcvideobufpool *pool = GST_QCVIDEOBUFPOOL (bpool);
  /* check pool configuration min/max etc .. here */
  allocator = gst_dmabuf_allocator_new ();

  if (pool->allocator)
    gst_object_unref (pool->allocator);
  if ((pool->allocator = allocator))
    gst_object_ref (allocator);

  return GST_BUFFER_POOL_CLASS (parent_class)->set_config (bpool, config);
}

static void
gst_qc_video_buffer_pool_finalize (GObject * object)
{
  Gstqcvideobufpool *pool = GST_QC_VIDEO_BUFFER_POOL_CAST (object);

  GST_LOG_OBJECT (pool, "finalize video buffer pool %p", pool);
  GST_DEBUG ("\n  gst_qc_video_buffer_pool_finalize \n");
  if (pool->allocator)
    gst_object_unref (pool->allocator);
  G_OBJECT_CLASS (gst_qcvideobufpool_parent_class)->finalize (object);
}

/* initialize the qcvideobufpool's class */
static void
gst_qcvideobufpool_class_init (GstqcvideobufpoolClass * klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;
  GstBufferPoolClass *gstbufferpool_class = (GstBufferPoolClass *) klass;

  gobject_class->finalize = gst_qc_video_buffer_pool_finalize;
  gstbufferpool_class->set_config = qc_video_buffer_pool_set_config;
  gstbufferpool_class->alloc_buffer = qc_video_buffer_pool_alloc_buffer;
  gstbufferpool_class->reset_buffer = qc_video_buffer_reset_buffer;
  gstbufferpool_class->free_buffer = qc_video_buffer_pool_free;
  GST_DEBUG ("\n gst_qcvideobufpool_class_init function \n");

}

/* initialize the new element
 * instantiate pads and add them to element
 * set pad calback functions
 * initialize instance structure
 */

static void
gst_qcvideobufpool_init (Gstqcvideobufpool * pool)
{
  GST_DEBUG ("\n gst_qcvideobufpool_init function\n");
}

/**
 * gst_qc_video_buffer_pool_new:
 *
 * Create a new bufferpool that can allocate video frames. This bufferpool
 * supports all the video bufferpool options.
 *
 * Returns: (transfer floating): a new #GstBufferPool to allocate video frames
 */
GstBufferPool *
gst_qc_video_buffer_pool_new ()
{
  Gstqcvideobufpool *pool;
  pool = g_object_new (GST_TYPE_QCVIDEOBUFPOOL, NULL);
  GST_DEBUG ("\n gst_qc_video_buffer_pool_new created \n");
  return GST_BUFFER_POOL_CAST (pool);
}
